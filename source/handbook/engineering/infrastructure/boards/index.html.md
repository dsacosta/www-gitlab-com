---
layout: markdown_page
title: "Boards"
---

## On this page
- TOC
{:toc}

## Boards

We use boards extensively to manage our work. 

### mStaff

The **[Infrastructure mStaff Board](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/boards/1240891)** collects issues for all managers (including the Director) and mStaff-level individual contributors (e.g., the Infrastructure Operations Analyst and Distinguished Engineer).

* **<https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/boards/1240891>**

#### Labels

The `Infrastructure mStaff` label will automatically add issues to the board. Lists are driven by the following labels:

| Label                        | List                                        | Focus          |
| ---------------------------- | ------------------------------------------- | -------------- |
| `Director of Infrastructure` | Director of Infrastructure                  | Infrastructure |
| `team::CI/CD & Enablement`   | Reliability Engineering: CI/CD & Enablement | Reliability    |
| `team::Dev & Ops`            | Reliability Engineering: Dev & ops          | Availability   |
| `team::Secure & Defend`      | Reliability Engineering: Secure and Defend  | Observability  |
| `team::Delivery`             | Delivery Engineering                        | Scalability    |
| `DE::Infrastructure`         | Distinguished Engineer, Infrastructure      | Infrastructure |
| `OA::Infrastructure`         | Operations Analyst, Infrastructure          | Cost           |

Other labels are relevant to issues in the board:

| Label             | Purpose                                                      |
| ----------------- | ------------------------------------------------------------ |
| `OKR`             | Denotes OKR-related issue. It is used to communicate status and progress for quarerly KRs as assigned to erach team. |
| `KPI`             | Denotes KPI-related issue. It is used to track progress on definition, implementation and tracking of Infrastructure KPIs. |
| `workflow::state` | Denotes the state of an issue according to our [workflow](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/labels?utf8=%E2%9C%93&subscribed=&search=workflow) conventions: `Ready`, `In Progress`, `Under Review`, `Blocked`, `Done`, and `Cancelled`. |

### Team Boards

#### Dev & Ops

#### Secure & Defend

#### CI/CD & Enablement

#### Delivery

#### Scalability

### Functional Boards 

#### Database

The **[Database Board](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1232123)** collects Database-related issues.

* **<https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1232123>**

The `Database` (*group label*) will automatically add issues to the board. 

##### List Labels

Board lists are driven by the following labels:

| Type    | Label                   | List                        | Notes                                      |
| ------- | ----------------------- | ----------------------------| ------------------------------------------ |
| *group* | `Ongres`                | Issues assigned to OnGres   |                                            |
| *group* | `Ongres::Support`       | OnGres support issues       | Support                                    |
| *group* | `Ongres::Project`       | OnGres project issues       | Project                                    |
| *group* | `Workflow::Ready`       | Issues ready to start       | Issues must always be prioritized          |
| *group* | `Workflow::In Progress` | Issues in progress          | Issues must always have a **Due Date**     |
| *group* | `Workflow::Blocked`     | Issues blocked              | Issues must describe what can unblock them |

##### Priority and Criticality Labels

Issues are labeled by priority and criticality. Priority incidetes what should be worked on first. Criticality describes the risk of not doing the work.

All issues must have priority and critically assigned to them. This is a requirement for issues in the `Workflow::Ready` state.

| Type    | Priority Label  | Description                                                                               | 
| ------- | --------------- | ----------------------------------------------------------------------------------------- |
| *group* | `P1`   | Highest priority items that require immediate action, with expected ETA in hours/days     |
| *group* | `P2`   | Items that require prompt attention, with expected ETA within the week                    |
| *group* | `P3`   | Items with expected ETA within the current milestone       |
| *group* | `P4`   | Items with expected ETA within the following milestone or beyond |


| Type    | Criticaility Label  | Description                                                                               | 
| ------- | ------------------- | ----------------------------------------------------------------------------------------- |
| *group* | `C1`   | Immediate threat to availability, performance or data durability  |
| *group* | `C2`   | Expected threat to availability and/or performance within 30 days |
| *group* | `C3`   | Expected threat to availability and/or performance within 60 days |
| *group* | `C4`   | Expected threat to availability and/or performance beyond 60 days |

Note: **data loss** is always a `C1` criticality.

All the issues with the Priority 1 `P1` label should be updated daily.

The time duration of our milestones is 2 weeks.

##### Logistics

The board is groomed **daily** by the Reliability Manager.

The manager's priorities are:

1. Ensure the `Workflow::Blocked` list is empty (i.e., unblocking issues is critical)
1. Maintain the board up to date with the help of issue assignees

#### Observability

The **[Observability Board](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/boards/1204134)** collects Observability-related issues.

* **<https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/boards/1204134>**

The `Obserbability` (*group label*) will automatically add issues to the board.

##### List Labels

Board lists are driven by the following labels:

| Type    | Label                   | List                        | Notes          |
| --------------------------------- | ----------------------------| -------------- |
| *group* | `Workflow::Ready`       | Issues ready to start       | Issues must always be prioritized          |
| *group* | `Workflow::In Progress` | Issues in progress          | Issues must always have a **Due Date**     |
| *group* | `Workflow::Blocked`     | Issues blocked              | Issues must describe what can unblock them |

##### Priority and Criticality Labels

Issues are labeled by priority and criticality. Priority incidetes what should be worked on first. Criticality describes the risk of not doing the work.

All issues must have priority and critically assigned to them. This is a requirement for issues in the `Workflow::Ready` state.

| Type    | Priority Label  | Description                                                                               |
| ------------------------- | ----------------------------------------------------------------------------------------- |
| *group* | `P1`   | Highest priority items that require immediate action, with expected ETA in hours/days     |
| *group* | `P2`   | Items that require prompt attention, with expected ETA within the week                    |
| *group* | `P3`   | Items with expected ETA within the current milestone       |
| *group* | `P4`   | Items with expected ETA within the following milestone or beyond |

| Type    | Criticaility Label  | Description                                                                               |
| *group* | `C1`   | Immediate threat to availability, performance or data durability |
| *group* | `C2`   | Expected threat to availability and/or performance within 30 days |
| *group* | `C3`   | Expected threat to availability and/or performance within 60 days |
| *group* | `C4`   | Expected threat to availability and/or performance beyond 60 days |

Note: **data loss** is always a `C1` criticality.

##### Logistics

The board is groomed **daily** by the Reliability Manager.

The manager's priorities are:

1. Ensure the `Workflow::Blocked` list is empty (i.e., unblocking issues is critical)
1. Maintain the board up to date with the help of issue assignees

