---
layout: markdown_page
title: "Isolation Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | October 7, 2019 |
| Target End Date | February 4, 2020|
| Slack           | [#wg_isolation](https://gitlab.slack.com/messages/CNTFF3RB5) (only accessible from within the company) |
| Google Doc      | [Isolation Working Group Agenda](https://docs.google.com/document/d/12PjNkkklBM3dIDXcDqlR6kXBanBbqbXu9LRmcjvuJgc/edit#) (only accessible from within the company) |
| Issue Board     | TBD             |

## Business Goal

To develop plan and practices for dedicated HW/Shards/Gitaly/Queues/Redis/Elasticsearch/Runner for paying customers on .com.  

## Exit Criteria (0%)

- Infrastructure has prioritized what would be most important for customers to have supported(?)
- Product has prioritized the most important item for incremental feature delivery(?)
- Development has scoped and sized the most important item for incremental execution(?)
- Quality has scoped and sized the most important item for regression validation(?)

## Roles and Responsibilities

| Working Group Role    | Person                | Title                            |
|-----------------------|-----------------------|----------------------------------|
| Executive Stakeholder | Christopher Lefelhocz | Senior Director of Development   |
| Facilitator           | Rachel Nienaber       | Engineering Manager, Geo         |
| Functional Lead       | Ramya Authappan       | Quality Engineering Manager, Dev |
| Functional Lead       | Fabian Zimmer         | Senior Product Manager, Geo      |
| Functional Lead       | Gerardo "Gerir" Lopez-Fernandez | Engineering Fellow, Infrastructure |
| Functional Lead       | Stan Hu               | Engineering Fellow, Development  |
| Member                | George Burdell        | Campus Alumni Representative     |
| Member                | Craig Gomes           | Engineering Manager, Memory      |
| Member                | Chun Du               | Director of Engineering, Enablement |
| Member                | Andrew Newdigate      | Distinguished Engineer, Infrastructure |
