---
layout: job_family_page
title: "VP, Information Systems"
---


## Responsibilities
1. Strategic and operational ownership for systems, business processes and data company wide.
1. Lead IT strategic and operational planning to achieve business goals.
1. Prioritize IT initiatives company wide.
1. Coordinate the evaluation, deployment, and management of current and future IT systems.
1. Identify opportunities for appropriate and cost-effective investment in information systems.
1. Evaluate and make recommendations on staffing, outsourcing, purchasing, and in-house development of systems and processes.
1. Responsible for information systems budget oversight and cost control.
1. Work in partnership with business functions to build a strategy and roadmap that will support rapid growth.
1. Participate in and facilitate the strategic and operational process improvement of the organization.
1. Coordinate project teams throughout the organization as it relates to Information Systems.
1. Identify potential bottlenecks and prepare the company for hyper-growth.

## Requirements
1. 4 years of experience as VP Information Technology, CIO or similar role.
1. Outstanding knowledge of modern tech stack of cloud applications.
1. Extensive hands on experience with design, development and implementation of IT systems.
1. Solid understanding of data analysis and data engineering in a hyper growth company.
1. Demonstrated knowledge of budgeting, financial planning and business operations.
1. Analytical mind and great problem-solving skills.
1. Have or had extensive experience with IT helpdesk function including hardware deployment and system access.
1. Outstanding communication and interpersonal abilities and history of success working with executives across an organization.
1. BSc/BA in computer science or relevant field. MSc/MA is a big plus.

## Performance Indicators
1. IT Helpdesk SLA achieved
1. All KPIs documented and running with data integrity
1. Company achieves IPO readiness on schedule



## Hiring Process
Candidates for this position can expect the hiring process to follow the order below.
1. Screening call with a team member from our recruiting team.
1. Interview with Director of Business Operations
1. Interview with 2+ Business Operations Department members
1. Interview with 1-2 executive team members
1. Interview with CEO plus 1-2 board members

Please note that a candidate may declined from the position at any stage of the process. 
Additional details about our process can be found on our [hiring page](/handbook/hiring). 
